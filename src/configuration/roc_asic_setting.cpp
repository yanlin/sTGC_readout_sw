#include "roc_asic_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ROC_ASIC_Setting::ROC_ASIC_Setting() :
  header(""),
  boardID(0),
  board_Type(0),
  ASIC_Type(2),
  chipID(0),
  command(""),
  UniqueS(""),
  ok(false)
{

  L1_first = 0;
  even_parity = 0;
  ROC_ID = 0;

  elink_speed_sROC0 = 0;
  elink_speed_sROC1 = 0;
  elink_speed_sROC2 = 0;
  elink_speed_sROC3 = 0;

  vmm_mask_sROC0 = 0;
  vmm_mask_sROC1 = 0;
  vmm_mask_sROC2 = 0;
  vmm_mask_sROC3 = 0;

  EOP_enable_sROC_mask = 0;
  NullEvt_enable_sROC_mask = 0;

  bypass = false;
  timeout_ena = false;

  TTC_start_bits = 0;

  enable_sROC_mask = 0;

  vmm_enable_mask = 0;
  timeout = 0;

  TX_current = 0;

  BC_offset = 0;
  BC_rollover = 0;

  eport_sROC0 = 0;
  eport_sROC1 = 0;
  eport_sROC2 = 0;
  eport_sROC3 = 0;

  fake_vmm_failure_mask = 0;
  TDS_enable_sROC_mask = 0;
  BUSY_enable_sROC_mask = 0;

  BUSY_On_Limit = 0;
  BUSY_Off_Limit = 0;

  Max_L1_Events_no_comma = 0;

}
void ROC_ASIC_Setting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
