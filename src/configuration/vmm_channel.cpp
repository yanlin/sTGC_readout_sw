#include "vmm_channel.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Channel
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
VMM_Channel::VMM_Channel() :
    number(0),
    //////////////////////////////////////////
    // VMM3 registers
    //////////////////////////////////////////
    sc(0),
    sl(0),
    st(0),
    sth(0),
    sm(0),
    sd(0),
    smx(0),
    sz10b(0),
    sz8b(0),
    sz6b(0),
    //////////////////////////////////////////
    // VMM2 registers
    //////////////////////////////////////////
    polarity(0),
    capacitance(0),
    leakage_current(0),
    test_pulse(0),
    hidden_mode(0),
    trim(0),
    monitor(0),
    s10bitADC(0),
    s8bitADC(0),
    s6bitADC(0),
    ok(false)
{
}
void VMM_Channel::print()
{
    stringstream ss;
    ss << "Channel " << number << endl;
    ss << "    > polarity         : " << polarity << endl;
    ss << "    > capacitance      : " << capacitance << endl;
    ss << "    > leakage current  : " << leakage_current << endl;
    ss << "    > test pulse       : " << test_pulse << endl;
    ss << "    > hidden mode      : " << hidden_mode << endl;
    ss << "    > trim             : " << trim << endl;
    ss << "    > monitor          : " << monitor << endl;
    ss << "    > s10bADC time set : " << s10bitADC << endl;
    ss << "    > s08bADC time set : " << s8bitADC << endl;
    ss << "    > s06bADC time set : " << s6bitADC << endl;
    cout << ss.str() << endl;
}
