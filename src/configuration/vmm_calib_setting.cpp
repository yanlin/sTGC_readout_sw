#include "vmm_calib_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  CalibSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
VMMCalibSetting::VMMCalibSetting() :
    /////////////////////////////////
    // VMM3 registers
    /////////////////////////////////
    ok(false)
{

  chan_baselines.resize(0);
  chan_baseline_vs_trim.resize(0);
  opt_trim_bits.resize(0);

}
void VMMCalibSetting::print()
{
}
