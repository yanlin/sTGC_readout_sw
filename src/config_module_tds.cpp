
// tds
#include "config_module_tds.h"
#include "vmm_decoder.h"
// std/stl
#include <iostream>
#include <bitset>
using namespace std;

// Qt
#include <QString>
#include <QDataStream>
#include <QByteArray>
#include <QBitArray>
#include <QStringList>

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_TDS
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
Config_TDS::Config_TDS(Config_Base *parent) :
  Config_Base(parent),
    //    m_dbg(false),
    //    m_socketHandler(0),
    m_configHandler(0)//,
    //    m_msg(0)
{
}
// ------------------------------------------------------------------------ //
Config_TDS& Config_TDS::LoadConfig(ConfigHandlerTDS& config)
{
    m_configHandler = &config;
    if(!m_configHandler) {
        msg()("FATAL ConfigHandler instance is null", "Config_TDS::LoadConfig", true);
        exit(1);
    }
    else if(dbg()) {
        msg()("ConfigHandler instance loaded", "Config_TDS::LoadConfig");
    }
    return *this;
}
//---------------------------------------------------// 
bool Config_TDS::SendTDSConfig(int send_to_port, const QHostAddress &target_ip)
{
  stringstream sx;
  bool send_ok = true;
  bool ok;

  sx.str("");
  //    sx << "sending message " << bit_stream.toStdString() << "\n";
  //    msg()(sx, "Config_TDS::SendConfig");

  //////////////////////////////////////////////////
  // build the configuration word(s) to be send to  
  // the front ends                                 
  //////////////////////////////////////////////////

  QByteArray datagram;
  QDataStream out(&datagram, QIODevice::WriteOnly);
  out.device()->seek(0);

  //////////////////////////////////////////////////
  // Global Registers                               
  //////////////////////////////////////////////////
  std::vector<QString> TDS_GlobalRegisters;
  TDS_GlobalRegisters.clear();
  fillTDSGlobalRegisters(TDS_GlobalRegisters);

  std::vector<QString> TDS_ChannelRegisters;
  TDS_ChannelRegisters.clear();
  fillTDSChannelRegisters(TDS_ChannelRegisters);

  std::vector<QString> TDS_LutRegisters;
  TDS_LutRegisters.clear();
  fillTDSLutRegisters(TDS_LutRegisters);

  //----------------------------------------------//

  out << (quint32) QString::fromStdString(config().TDSGlobalSettings().header).toUInt(&ok,16);
  out << (quint16) Build_Mask(config().TDSGlobalSettings().board_Type, config().TDSGlobalSettings().boardID,
			      config().TDSGlobalSettings().ASIC_Type,  config().TDSGlobalSettings().chipID).toUInt(&ok,16);
  out << (quint16) QString::fromStdString(config().TDSGlobalSettings().command).toUInt(&ok,16);

  //  sx.str("");
  //  sx << config().TDSMap().toStdString() << "\n";
  //  msg()(sx,"Config_TDS::SendConfig");

  //------------------------------------------------------//

  // 32 bits of BCID offset, rollover, clock phase, matching window (MSB first)
  out << (quint32)(TDS_GlobalRegisters.at(0).toUInt(&ok,2));
  // 16 bits of VMM1/VMM0 clock phase, SER PLL current/resistor (MSB first)
  out << (quint16)(TDS_GlobalRegisters.at(1).toUInt(&ok,2));

  // 128 bits of channel enable / disable (MSB first, chan 128 first)
  for( int i=0; i<TDS_ChannelRegisters.at(0).size(); i=i+16) {
    QString tmp_16byte = TDS_ChannelRegisters.at(0).mid(i,16) ;
    out << (quint16) tmp_16byte.toUInt(&ok,2);
  }

  // 16*16=256 bits of LUT bandID/lead strip (MSB first, LUTf first)
  for( int i=0; i<TDS_LutRegisters.at(0).size(); i=i+16) {
    QString tmp_16byte = TDS_LutRegisters.at(0).mid(i,16) ;
    out << (quint16) tmp_16byte.toUInt(&ok,2);
  }

  // 8*104=832 bits of pad channel delay (MSB first, chan 104 first)
  for( int i=0; i<TDS_ChannelRegisters.at(1).size(); i=i+16) {
    QString tmp_16byte = TDS_ChannelRegisters.at(1).mid(i,16) ;
    out << (quint16) tmp_16byte.toUInt(&ok,2);
  }

  // 32 bits of bypass, reset, prompt circuit and reject window (MSB first)
  out << (quint32)(TDS_GlobalRegisters.at(2).toUInt(&ok,2));

  //--------------------------------------------------------------//

  QString tmp = QString::fromStdString( datagram.toHex().toStdString() );
  send_ok = SendConfigN(send_to_port, target_ip, tmp, 1 );

  //boost::this_thread::sleep(boost::posix_time::milliseconds(1000));

  return send_ok;

  //--------------------------------------------------------------//

}
// ------------------------------------------------------------------------ //
void Config_TDS::fillTDSGlobalRegisters(std::vector<QString> &global)
{
  stringstream sx;
  if(dbg()) msg()("Loading global registers","Config_TDS::fillGlobalRegisters");
  global.clear();
  int pos = 0;

  bool ok=true;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  QString str;
  //str = QString("Decimal 4 is %1 in binary")
  //  .arg(4, 7, 2, QChar('0'));
  // str == "Decimal 4 is 0000100 in binary"     (4 in base 10 is writen as 0000100 in 7 binary bits (pad with QChar('0'))"
  //sx.str("");
  //sx << str.toStdString() << "\n";
  //msg()(sx);

  //-----------------------------------//
  //          first 32 bits
  //-----------------------------------//

  QString spi1_0 = bit32_empty;
  pos = 0;

  str = QString("%1").arg(config().TDSGlobalSettings().bcid_offset, 12, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().bcid_rollover,12, 2, QChar('0'));
  spi1_0.replace(pos, str.size(), str);
  pos+=str.size();

  if ( config().TDSGlobalSettings().bcid_clockphase == 0 ) spi1_0.replace(pos, 4, "0011"); // 0 degree
  if ( config().TDSGlobalSettings().bcid_clockphase == 1 ) spi1_0.replace(pos, 4, "0110"); // 90 degree
  if ( config().TDSGlobalSettings().bcid_clockphase == 2 ) spi1_0.replace(pos, 4, "1100"); // 180 degree
  if ( config().TDSGlobalSettings().bcid_clockphase == 3 ) spi1_0.replace(pos, 4, "1001"); // 270 degree
  pos+=4;

  if ( config().TDSGlobalSettings().match_window == 0 ) spi1_0.replace(pos, 4, "0000"); // 25    ns    
  if ( config().TDSGlobalSettings().match_window == 1 ) spi1_0.replace(pos, 4, "0001"); // 31.25 ns   
  if ( config().TDSGlobalSettings().match_window == 2 ) spi1_0.replace(pos, 4, "0011"); // 37.5  ns  
  if ( config().TDSGlobalSettings().match_window == 3 ) spi1_0.replace(pos, 4, "0111"); // 43.75 ns  
  if ( config().TDSGlobalSettings().match_window == 4 ) spi1_0.replace(pos, 4, "1111"); // 50    ns  
  pos+=4;

  //---------------------------------//
  //            bit 32-47            //
  //---------------------------------//

  QString spi1_1 = bit16_empty;
  pos = 0;

  str =QString("%1").arg(config().TDSGlobalSettings().vmm1_clockphase, 5, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().vmm0_clockphase, 5, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().SER_PLL_current, 4, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().SER_PLL_resistor, 2, 2, QChar('0'));
  spi1_1.replace(pos, str.size(), str);
  pos+=str.size();

  //---------------------------------//
  //          bit 1264-1295          //
  //---------------------------------//

  QString spi1_2 = bit32_empty;
  pos = 0;

  // 8 bits for reject window
  str =QString("%1").arg(config().TDSGlobalSettings().reject_window, 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  // 4 bits for bypass prompt circuit 
  QString hexadecimalNumber = "0x0";
  if ( config().TDSGlobalSettings().bypass_prompt == 0 ) hexadecimalNumber = "0x0";; // don't bypass
  if ( config().TDSGlobalSettings().bypass_prompt == 1 ) hexadecimalNumber = "0xF";; // bypass      
  str = QString("%1").arg(hexadecimalNumber.toInt(&ok, 16), 4, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  // 4 bits for prompt circuit 
  str =QString("%1").arg(config().TDSGlobalSettings().prompt_circuit, 4, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  // 3 bits filler + 5 bits of bypass and enable/disable
  str =QString("000");
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().bypass_trigger, 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().bypass_scrambler, 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().test_frame2Router_enable, 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().stripTDS_globaltest, 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  str =QString("%1").arg(config().TDSGlobalSettings().PRBS_en, 1, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  // 8 bit for reset
  hexadecimalNumber = "0x0";
  if( config().TDSGlobalSettings().soft_resets == 0) hexadecimalNumber = "0x0";  // no resets
  if( config().TDSGlobalSettings().soft_resets == 1) hexadecimalNumber = "0x14"; // reset SER 
  if( config().TDSGlobalSettings().soft_resets == 2) hexadecimalNumber = "0x06"; // reset logic   
  if( config().TDSGlobalSettings().soft_resets == 3) hexadecimalNumber = "0x20"; // reset ePLL
  str = QString("%1").arg(hexadecimalNumber.toInt(&ok, 16), 8, 2, QChar('0'));
  spi1_2.replace(pos, str.size(), str);
  pos+=str.size();

  global.push_back(spi1_0);
  global.push_back(spi1_1);
  global.push_back(spi1_2);

}

void Config_TDS::fillTDSChannelRegisters(std::vector<QString> &channel)
{
  stringstream sx;
  if(dbg()) msg()("Loading TDS channel registers","Config_TDS::fillChannelRegisters");
  channel.clear();
  int pos = 0;
  
  //  bool ok=true;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  QString str;
  str = QString("Decimal 4 is %1 in binary")
    .arg(4, 7, 2, QChar('0'));
  // str == "Decimal 4 is 0000100 in binary"     (4 in base 10 is writen as 0000100 in 7 binary bits (pad with QChar('0'))"

  sx.str("");
  sx << str.toStdString() << "\n";
  msg()(sx);

  //-------------------------------------------//
  //      channel enable/disable   (128 bits)
  //-------------------------------------------//

  QString spi1_0 = "";
  for (int i=0; i< 4; i++) {
    spi1_0 += bit32_empty;
  }
  pos = 0;

  // start from channel 128 to channel 1
  for ( int i = 127; i >=0; i-- ) {

    uint32_t outbit = 0;
    if ( config().TDS_ChannelSettings(i).enabled ) outbit = 0; // output 0 for enabled
    else                                           outbit = 1; // output 1 for disabled
    str = QString("%1").arg(outbit, 1, 2, QChar('0'));
    spi1_0.replace(pos, str.size(), str);
    pos+=str.size();

  }

  //--------------------------------------------------------//
  //    Pad channel delay 8 bits x 104 channels 
  //    first 5 is actual delay, 3 is filler per channel
  //--------------------------------------------------------//

  QString spi1_1 = "";
  for (int i=0; i< 26; i++) {
    spi1_1 += bit32_empty;
  }
  pos = 0;

  for ( int j = 0; j <= 6; j++ ) { 
    for ( int i = 15; i >= 0; i--) {

      int ichan = j*16+i;
      if (ichan >= 104) continue;

      int delay_i = config().TDS_ChannelSettings(ichan).delay;
      uint32_t outbit = 0; 
      if ( delay_i == 0 ) outbit = 0b10011; //  0.000 ns
      if ( delay_i == 1 ) outbit = 0b00011; //  3.125 ns
      if ( delay_i == 2 ) outbit = 0b10110; //  6.250 ns
      if ( delay_i == 3 ) outbit = 0b00110; //  9.375 ns
      if ( delay_i == 4 ) outbit = 0b11100; // 12.5   ns
      if ( delay_i == 5 ) outbit = 0b01100; // 15.625 ns
      if ( delay_i == 6 ) outbit = 0b11001; // 18.75  ns
      if ( delay_i == 7 ) outbit = 0b01001; // 21.875 ns
      if ( delay_i >= 8 ) std::cout << "wtf!!! wtf!!! pad delay in tds not recognized" << std::endl;
      
      //str  = QString("000");
      str = QString("%1").arg( outbit, 8, 2, QChar('0'));
      spi1_1.replace(pos, str.size(), str);
      pos+=str.size();
    }
  }

  channel.push_back(spi1_0);
  channel.push_back(spi1_1);
}

// ------------------------------------------------------------------------ //
void Config_TDS::fillTDSLutRegisters(std::vector<QString> &lut)
{
  stringstream sx;
  if(dbg()) msg()("Loading lut registers","Config_TDS::fillTDSLutRegisters");
  lut.clear();
  int pos = 0;

  // bool ok=true;

  QString bit32_empty = "00000000000000000000000000000000";
  QString bit16_empty = "0000000000000000";

  QString str;
  str = QString("Decimal 4 is %1 in binary")
    .arg(4, 7, 2, QChar('0'));
  // str == "Decimal 4 is 0000100 in binary"     (4 in base 10 is writen as 0000100 in 7 binary bits (pad with QChar('0'))"           

  sx.str("");
  sx << str.toStdString() << "\n";
  // msg()(sx);

  //----------------------------------------------------------------//
  //          LUT BandID and LeadStrip
  //  16 bits x 16 LUT (1 filler, 8 for BandID and 7 for leadstrip )
  //-----------------------------------------------------------------//

  QString spi1_0 = "";
  for (int i=0; i< 8; i++) {
    spi1_0 += bit32_empty;
  }
  pos = 0;

  for ( int i = 7; i >= 0; i-- ) {
    str = QString("%1").arg(config().TDS_LUTSettings(i).BandID, 9, 2, QChar('0'));
    spi1_0.replace(pos, str.size(), str);
    pos+=str.size();
    str = QString("%1").arg(config().TDS_LUTSettings(i).LeadStrip, 7, 2, QChar('0'));
    spi1_0.replace(pos, str.size(), str);
    pos+=str.size();
  }

  for ( int i = 15; i >= 8; i-- ) {
    str = QString("%1").arg(config().TDS_LUTSettings(i).BandID, 9, 2, QChar('0'));
    spi1_0.replace(pos, str.size(), str);
    pos+=str.size();
    str = QString("%1").arg(config().TDS_LUTSettings(i).LeadStrip, 7, 2, QChar('0'));
    spi1_0.replace(pos, str.size(), str);
    pos+=str.size();
  }

  lut.push_back(spi1_0);

}
