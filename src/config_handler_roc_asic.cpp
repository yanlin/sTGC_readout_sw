// vmm
#include "config_handler_roc_asic.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>

//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandler ROC ASIC -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerRocASIC::ConfigHandlerRocASIC(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0)
{
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocASIC::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocASIC::SetBoardID( int boardID ){
  m_roc_asic_settings.boardID = boardID;
}
void ConfigHandlerRocASIC::SetBoardType( int boardType ){
  m_roc_asic_settings.board_Type = boardType;
}

bool ConfigHandlerRocASIC::Load_ROC_ASICConfig_from_File(const QString &filename)
{

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);

  // Load the global configuration
  m_roc_asic_settings         = Load_ROC_ASIC_Settings(pt);
  m_roc_asic_analog_epll_VMM0 = Load_ROC_ASIC_Ana_VMM0_Config(pt);
  m_roc_asic_analog_epll_VMM1 = Load_ROC_ASIC_Ana_VMM1_Config(pt);
  m_roc_asic_analog_epll_TDS  = Load_ROC_ASIC_Ana_TDS_Config(pt);
  m_roc_asic_analog_epll_int  = Load_ROC_ASIC_Ana_TDS_Config(pt);

  if(!m_roc_asic_settings.ok) {
    msg()("Problem loading ROC ASIC Settings", "ConfigHandlerRocASIC::Load_Roc_ASICConfig_from_File");
  }
  if(!m_roc_asic_analog_epll_VMM0.ok) {
    msg()("Problem loading ROC ASIC epll VMM0 Settings", "ConfigHandlerRocASIC::Load_Roc_ASICConfig_from_File");
  }
  if(!m_roc_asic_analog_epll_VMM1.ok) {
    msg()("Problem loading ROC ASIC epll VMM1 Settings", "ConfigHandlerRocASIC::Load_Roc_ASICConfig_from_File");
  }
  if(!m_roc_asic_analog_epll_TDS.ok) {
    msg()("Problem loading ROC ASIC epll TDS Settings", "ConfigHandlerRocASIC::Load_Roc_ASICConfig_from_File");
  }
  if(!m_roc_asic_analog_epll_int.ok) {
    msg()("Problem loading ROC ASIC epll Int Settings", "ConfigHandlerRocASIC::Load_Roc_ASICConfig_from_File");
  }

  return ( m_roc_asic_settings.ok && 
	   m_roc_asic_analog_epll_VMM0.ok && m_roc_asic_analog_epll_VMM1.ok &&
	   m_roc_asic_analog_epll_TDS.ok && m_roc_asic_analog_epll_int.ok ); //daq_ok && m_vmmMap.ok && vmmchan_ok && clocks_ok);
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocASIC::Write_ROC_ASICConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;   

  ptree out_root;

  // ----------------------------------------------- //
  //  global settings
  // ----------------------------------------------- //

  ptree out_global;
  out_global.put("header",               ROC_ASIC_Settings().header);
  out_global.put("board_Type",           ROC_ASIC_Settings().board_Type);
  out_global.put("boardID",              ROC_ASIC_Settings().boardID);
  out_global.put("ASIC_Type",            ROC_ASIC_Settings().ASIC_Type);
  out_global.put("chipID",               ROC_ASIC_Settings().chipID);
  out_global.put("command",              ROC_ASIC_Settings().command);
  out_global.put("UniqueS",              ROC_ASIC_Settings().UniqueS);

  out_global.put("L1_first",             ROC_ASIC_Settings().L1_first);
  out_global.put("even_parity",          ROC_ASIC_Settings().even_parity);
  out_global.put("ROC_ID",               ROC_ASIC_Settings().ROC_ID);

  out_global.put("elink_speed_sROC0",    ROC_ASIC_Settings().elink_speed_sROC0);
  out_global.put("elink_speed_sROC1",    ROC_ASIC_Settings().elink_speed_sROC1);
  out_global.put("elink_speed_sROC2",    ROC_ASIC_Settings().elink_speed_sROC2);
  out_global.put("elink_speed_sROC3",    ROC_ASIC_Settings().elink_speed_sROC3);

  out_global.put("vmm_mask_sROC0",       ROC_ASIC_Settings().vmm_mask_sROC0);
  out_global.put("vmm_mask_sROC1",       ROC_ASIC_Settings().vmm_mask_sROC1);
  out_global.put("vmm_mask_sROC2",       ROC_ASIC_Settings().vmm_mask_sROC2);
  out_global.put("vmm_mask_sROC3",       ROC_ASIC_Settings().vmm_mask_sROC3);

  out_global.put("EOP_enable_sROC_mask",     ROC_ASIC_Settings().EOP_enable_sROC_mask);
  out_global.put("NullEvt_enable_sROC_mask", ROC_ASIC_Settings().NullEvt_enable_sROC_mask);
  out_global.put("bypass",                   ROC_ASIC_Settings().bypass);
  out_global.put("timeout_ena",              ROC_ASIC_Settings().timeout_ena);
  out_global.put("TTC_start_bits",           ROC_ASIC_Settings().TTC_start_bits);
  out_global.put("enable_sROC_mask",         ROC_ASIC_Settings().enable_sROC_mask);

  out_global.put("vmm_enable_mask",          ROC_ASIC_Settings().vmm_enable_mask);
  out_global.put("timeout",                  ROC_ASIC_Settings().timeout);
  out_global.put("TX_current",               ROC_ASIC_Settings().TX_current);
  out_global.put("BC_offset",                ROC_ASIC_Settings().BC_offset);
  out_global.put("BC_rollover",              ROC_ASIC_Settings().BC_rollover);
  out_global.put("eport_sROC0",              ROC_ASIC_Settings().eport_sROC0);
  out_global.put("eport_sROC1",              ROC_ASIC_Settings().eport_sROC1);
  out_global.put("eport_sROC2",              ROC_ASIC_Settings().eport_sROC2);
  out_global.put("eport_sROC3",              ROC_ASIC_Settings().eport_sROC3);
  out_global.put("fake_vmm_failure_mask",    ROC_ASIC_Settings().fake_vmm_failure_mask);
  out_global.put("TDS_enable_sROC_mask",     ROC_ASIC_Settings().TDS_enable_sROC_mask);
  out_global.put("BUSY_enable_sROC_mask",    ROC_ASIC_Settings().BUSY_enable_sROC_mask);
  out_global.put("BUSY_On_Limit",            ROC_ASIC_Settings().BUSY_On_Limit);
  out_global.put("BUSY_Off_Limit",           ROC_ASIC_Settings().BUSY_Off_Limit);
  out_global.put("Max_L1_Events_no_comma",   ROC_ASIC_Settings().Max_L1_Events_no_comma);

  out_global.put("tp_bypass_global",         ROC_ASIC_Settings().tp_bypass_global);
  out_global.put("tp_phase_global",          ROC_ASIC_Settings().tp_phase_global);
  out_global.put("LockOutInv",               ROC_ASIC_Settings().LockOutInv);
  out_global.put("testOutEn",                ROC_ASIC_Settings().testOutEn);
  out_global.put("testOutMux",               ROC_ASIC_Settings().testOutMux);

  out_global.put("VMM_BCR_INV",              ROC_ASIC_Settings().VMM_BCR_INV);
  out_global.put("VMM_ENA_INV",              ROC_ASIC_Settings().VMM_ENA_INV);
  out_global.put("VMM_L0_INV",               ROC_ASIC_Settings().VMM_L0_INV);
  out_global.put("VMM_TP_INV",               ROC_ASIC_Settings().VMM_TP_INV);

  out_global.put("TDS_BCR_INV",              ROC_ASIC_Settings().TDS_BCR_INV);

  // ----------------------------------------------- //

  ptree out_vmm0 = ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_Ana_VMM0_Settings());
  ptree out_vmm1 = ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_Ana_VMM1_Settings());
  ptree out_tds  = ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_Ana_TDS_Settings());
  ptree out_int  = ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_Ana_Int_Settings());

  // ----------------------------------------------- //
  // stitch together the fields
  // ----------------------------------------------- //
  out_root.add_child("digital_registers", out_global);
  out_root.add_child("vmm0_epll_registers", out_vmm0);
  out_root.add_child("vmm1_epll_registers", out_vmm1);
  out_root.add_child("tds_epll_registers",  out_tds);
  out_root.add_child("int_epll_registers",  out_int);

  // put everything under a global node
  outpt.add_child("configuration", out_root);


  stringstream sx;
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
	      boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerRocASIC::WriteConfig");
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerRocASIC::WriteConfig");
    return;
  }

  return;

}
// -------------------------------------------------------------------------- //
boost::property_tree::ptree ConfigHandlerRocASIC::ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_analog_epll & Analog_ePll_obj)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;

  ptree out_root;

  // ----------------------------------------------- //
  //  global settings                                  
  // ----------------------------------------------- //
  ptree out_global;
  out_global.put("ePllPhase40MHz_0",  Analog_ePll_obj.ePllPhase40MHz_0);
  out_global.put("ePllPhase40MHz_1",  Analog_ePll_obj.ePllPhase40MHz_1);
  out_global.put("ePllPhase40MHz_2",  Analog_ePll_obj.ePllPhase40MHz_2);
  out_global.put("ePllPhase40MHz_3",  Analog_ePll_obj.ePllPhase40MHz_3);
  out_global.put("ePllPhase160MHz_0", Analog_ePll_obj.ePllPhase160MHz_0);
  out_global.put("ePllPhase160MHz_1", Analog_ePll_obj.ePllPhase160MHz_1);
  out_global.put("ePllPhase160MHz_2", Analog_ePll_obj.ePllPhase160MHz_2);
  out_global.put("ePllPhase160MHz_3", Analog_ePll_obj.ePllPhase160MHz_3);
  out_global.put("ePllInstantLock",   Analog_ePll_obj.ePllInstantLock);
  out_global.put("ePllReset",         Analog_ePll_obj.ePllReset);

  out_global.put("bypassPll",         Analog_ePll_obj.bypassPll);
  out_global.put("ePllLockEn",        Analog_ePll_obj.ePllLockEn);
  out_global.put("ePll_Ref_Freq",     Analog_ePll_obj.ePll_Ref_Freq);
  out_global.put("ePllEnablePhase",   Analog_ePll_obj.ePllEnablePhase);
  out_global.put("ePll_lcp",          Analog_ePll_obj.ePll_lcp);
  out_global.put("ePll_cap",          Analog_ePll_obj.ePll_cap);
  out_global.put("ePll_res",          Analog_ePll_obj.ePll_res);

  out_global.put("tp_phase_0",        Analog_ePll_obj.tp_phase_0);
  out_global.put("tp_phase_1",        Analog_ePll_obj.tp_phase_1);
  out_global.put("tp_phase_2",        Analog_ePll_obj.tp_phase_2);
  out_global.put("tp_phase_3",        Analog_ePll_obj.tp_phase_3);
  out_global.put("tp_bypass_mask",    Analog_ePll_obj.tp_bypass_mask);

  out_global.put("ctrl_phase_0",      Analog_ePll_obj.ctrl_phase_0);
  out_global.put("ctrl_phase_1",      Analog_ePll_obj.ctrl_phase_1);
  out_global.put("ctrl_phase_2",      Analog_ePll_obj.ctrl_phase_2);
  out_global.put("ctrl_phase_3",      Analog_ePll_obj.ctrl_phase_3);
  out_global.put("ctrl_bypass_mask",  Analog_ePll_obj.ctrl_bypass_mask);

  out_global.put("ctrl_delay_0",      Analog_ePll_obj.ctrl_delay_0);
  out_global.put("ctrl_delay_1",      Analog_ePll_obj.ctrl_delay_1);
  out_global.put("ctrl_delay_2",      Analog_ePll_obj.ctrl_delay_2);
  out_global.put("ctrl_delay_3",      Analog_ePll_obj.ctrl_delay_3);
  out_global.put("ctrl_delay_mask",   Analog_ePll_obj.ctrl_delay_mask);

  out_global.put("tx_enable",         Analog_ePll_obj.tx_enable);
  out_global.put("tx_csel_enable",    Analog_ePll_obj.tx_csel_enable);

  return out_global;
}
//// ------------------------------------------------------------------------ //
ROC_ASIC_Setting ConfigHandlerRocASIC::Load_ROC_ASIC_Settings(const boost::property_tree::ptree& pt)
{
  using boost::property_tree::ptree;

  stringstream sx;

  ROC_ASIC_Setting g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {
      if(!(conf.first == "digital_registers")) continue;

      g.header          = conf.second.get<string>("header");
      g.board_Type      = conf.second.get<int>("board_Type");
      g.boardID         = conf.second.get<int>("boardID");
      g.ASIC_Type       = conf.second.get<int>("ASIC_Type");
      g.chipID          = conf.second.get<int>("chipID");
      g.command         = conf.second.get<string>("command");
      g.UniqueS         = conf.second.get<string>("UniqueS");


      g.L1_first             = conf.second.get<uint32_t>("L1_first");
      g.even_parity          = conf.second.get<uint32_t>("even_parity");
      g.ROC_ID               = conf.second.get<uint32_t>("ROC_ID");

      g.elink_speed_sROC0    = conf.second.get<uint32_t>("elink_speed_sROC0");
      g.elink_speed_sROC1    = conf.second.get<uint32_t>("elink_speed_sROC1");
      g.elink_speed_sROC2    = conf.second.get<uint32_t>("elink_speed_sROC2");
      g.elink_speed_sROC3    = conf.second.get<uint32_t>("elink_speed_sROC3");

      g.vmm_mask_sROC0       = conf.second.get<uint32_t>("vmm_mask_sROC0");
      g.vmm_mask_sROC1       = conf.second.get<uint32_t>("vmm_mask_sROC1");
      g.vmm_mask_sROC2       = conf.second.get<uint32_t>("vmm_mask_sROC2");
      g.vmm_mask_sROC3       = conf.second.get<uint32_t>("vmm_mask_sROC3");

      g.EOP_enable_sROC_mask     = conf.second.get<uint32_t>("EOP_enable_sROC_mask");
      g.NullEvt_enable_sROC_mask = conf.second.get<uint32_t>("NullEvt_enable_sROC_mask");
      g.bypass                   = conf.second.get<bool>("bypass");
      g.timeout_ena              = conf.second.get<bool>("timeout_ena");
      g.TTC_start_bits           = conf.second.get<uint32_t>("TTC_start_bits");
      g.enable_sROC_mask         = conf.second.get<uint32_t>("enable_sROC_mask");

      g.vmm_enable_mask          = conf.second.get<uint32_t>("vmm_enable_mask");
      g.timeout                  = conf.second.get<uint32_t>("timeout");
      g.TX_current               = conf.second.get<uint32_t>("TX_current");
      g.BC_offset                = conf.second.get<uint32_t>("BC_offset");
      g.BC_rollover              = conf.second.get<uint32_t>("BC_rollover");
      g.eport_sROC0              = conf.second.get<uint32_t>("eport_sROC0");
      g.eport_sROC1              = conf.second.get<uint32_t>("eport_sROC1");
      g.eport_sROC2              = conf.second.get<uint32_t>("eport_sROC2");
      g.eport_sROC3              = conf.second.get<uint32_t>("eport_sROC3");
      g.fake_vmm_failure_mask    = conf.second.get<uint32_t>("fake_vmm_failure_mask");
      g.TDS_enable_sROC_mask     = conf.second.get<uint32_t>("TDS_enable_sROC_mask");
      g.BUSY_enable_sROC_mask    = conf.second.get<uint32_t>("BUSY_enable_sROC_mask");
      g.BUSY_On_Limit            = conf.second.get<uint32_t>("BUSY_On_Limit");
      g.BUSY_Off_Limit           = conf.second.get<uint32_t>("BUSY_Off_Limit");
      g.Max_L1_Events_no_comma   = conf.second.get<uint32_t>("Max_L1_Events_no_comma");

      g.tp_bypass_global         = conf.second.get<uint32_t>("tp_bypass_global");
      g.tp_phase_global          = conf.second.get<uint32_t>("tp_phase_global");
      g.LockOutInv               = conf.second.get<uint32_t>("LockOutInv");
      g.testOutEn                = conf.second.get<uint32_t>("testOutEn");
      g.testOutMux               = conf.second.get<uint32_t>("testOutMux");

      g.VMM_BCR_INV              = conf.second.get<uint32_t>("VMM_BCR_INV");
      g.VMM_ENA_INV              = conf.second.get<uint32_t>("VMM_ENA_INV");
      g.VMM_L0_INV               = conf.second.get<uint32_t>("VMM_L0_INV");
      g.VMM_TP_INV               = conf.second.get<uint32_t>("VMM_TP_INV");

      g.TDS_BCR_INV              = conf.second.get<uint32_t>("TDS_BCR_INV");


     }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading ROC ASIC registers: " << e.what();
    msg()(sx,"ConfigHandlerRocASIC::Load_Roc_ASICSettings");
    outok = false;
  } 

  g.ok = outok;

  m_roc_asic_settings = g;

  return g;

}
// -------------------------------------------------------------------------- //
ROC_ASIC_analog_epll ConfigHandlerRocASIC::Load_ROC_ASIC_Ana_VMM0_Config(const boost::property_tree::ptree& p){
  return Load_ROC_ASIC_Ana_ePll(p, VMM1_ePll);
}
ROC_ASIC_analog_epll ConfigHandlerRocASIC::Load_ROC_ASIC_Ana_VMM1_Config(const boost::property_tree::ptree& p) {
  return Load_ROC_ASIC_Ana_ePll(p, VMM1_ePll);
}
ROC_ASIC_analog_epll ConfigHandlerRocASIC::Load_ROC_ASIC_Ana_TDS_Config( const boost::property_tree::ptree& p) {
  return Load_ROC_ASIC_Ana_ePll(p, TDS_ePll);
}
ROC_ASIC_analog_epll ConfigHandlerRocASIC::Load_ROC_ASIC_Ana_Int_Config( const boost::property_tree::ptree& p) {
  return Load_ROC_ASIC_Ana_ePll(p, Int_ePll);
}
// -------------------------------------------------------------------------- //
ROC_ASIC_analog_epll ConfigHandlerRocASIC::Load_ROC_ASIC_Ana_ePll(const boost::property_tree::ptree& pt, int iEPll)
{
  using boost::property_tree::ptree;

  stringstream sx;

  ROC_ASIC_analog_epll g;

  bool outok = true;

  try{
    for(const auto& conf : pt.get_child("configuration")) {

      if( iEPll == VMM0_ePll && !(conf.first == "vmm0_epll_registers")) continue;
      if( iEPll == VMM1_ePll && !(conf.first == "vmm1_epll_registers")) continue;
      if( iEPll == TDS_ePll  && !(conf.first == "tds_epll_registers")) continue;
      if( iEPll == Int_ePll  && !(conf.first == "int_epll_registers")) continue;

      g.ePllPhase40MHz_0  = conf.second.get<uint32_t>("ePllPhase40MHz_0");
      g.ePllPhase40MHz_1  = conf.second.get<uint32_t>("ePllPhase40MHz_1");
      g.ePllPhase40MHz_2  = conf.second.get<uint32_t>("ePllPhase40MHz_2");
      g.ePllPhase40MHz_3  = conf.second.get<uint32_t>("ePllPhase40MHz_3");

      g.ePllPhase160MHz_0 = conf.second.get<uint32_t>("ePllPhase160MHz_0");
      g.ePllPhase160MHz_1 = conf.second.get<uint32_t>("ePllPhase160MHz_1");
      g.ePllPhase160MHz_2 = conf.second.get<uint32_t>("ePllPhase160MHz_2");
      g.ePllPhase160MHz_3 = conf.second.get<uint32_t>("ePllPhase160MHz_3");

      g.ePllInstantLock   = conf.second.get<bool>("ePllInstantLock");
      g.ePllReset         = conf.second.get<bool>("ePllReset");
      g.bypassPll         = conf.second.get<bool>("bypassPll");
      g.ePllLockEn        = conf.second.get<bool>("ePllLockEn");

      g.ePll_Ref_Freq     = conf.second.get<uint32_t>("ePll_Ref_Freq");
      g.ePllEnablePhase   = conf.second.get<uint32_t>("ePllEnablePhase");
      g.ePll_lcp          = conf.second.get<uint32_t>("ePll_lcp");
      g.ePll_cap          = conf.second.get<uint32_t>("ePll_cap");
      g.ePll_res          = conf.second.get<uint32_t>("ePll_res");

      g.tp_phase_0        = conf.second.get<uint32_t>("tp_phase_0");
      g.tp_phase_1        = conf.second.get<uint32_t>("tp_phase_1");
      g.tp_phase_2        = conf.second.get<uint32_t>("tp_phase_2");
      g.tp_phase_3        = conf.second.get<uint32_t>("tp_phase_3");
      g.tp_bypass_mask    = conf.second.get<uint32_t>("tp_bypass_mask");

      g.ctrl_phase_0      = conf.second.get<uint32_t>("ctrl_phase_0");
      g.ctrl_phase_1      = conf.second.get<uint32_t>("ctrl_phase_1");
      g.ctrl_phase_2      = conf.second.get<uint32_t>("ctrl_phase_2");
      g.ctrl_phase_3      = conf.second.get<uint32_t>("ctrl_phase_3");
      g.ctrl_bypass_mask  = conf.second.get<uint32_t>("ctrl_bypass_mask");

      g.ctrl_delay_0      = conf.second.get<uint32_t>("ctrl_delay_0");
      g.ctrl_delay_1      = conf.second.get<uint32_t>("ctrl_delay_1");
      g.ctrl_delay_2      = conf.second.get<uint32_t>("ctrl_delay_2");
      g.ctrl_delay_3      = conf.second.get<uint32_t>("ctrl_delay_3");
      g.ctrl_delay_mask   = conf.second.get<uint32_t>("ctrl_delay_mask");

      g.tx_enable         = conf.second.get<uint32_t>("tx_enable");
      g.tx_csel_enable    = conf.second.get<uint32_t>("tx_csel_enable");

    }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading ROC ASIC registers: " << e.what();
    msg()(sx,"ConfigHandlerRocASIC::Load_Roc_ASICSettings");
    outok = false;
  }

  g.ok = outok;

  if ( iEPll == VMM0_ePll ) m_roc_asic_analog_epll_VMM0 = g;
  if ( iEPll == VMM1_ePll ) m_roc_asic_analog_epll_VMM1 = g;
  if ( iEPll == TDS_ePll )  m_roc_asic_analog_epll_TDS  = g;
  if ( iEPll == Int_ePll )  m_roc_asic_analog_epll_int  = g;

  return g;

}
//// ------------------------------------------------------------------------ //                     
QString ConfigHandlerRocASIC::ROC_ASICMask(){

  QString MapString = "0000000000000000";
  QString str;
  int pos = 0;

  str = QString("%1").arg(m_roc_asic_settings.board_Type, 2, 2, QChar('0'));
  MapString.replace(1, str.size(), str);
  pos+=1+str.size();

  str = QString("%1").arg(m_roc_asic_settings.boardID, 3, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=pos+str.size();

  str = QString("%1").arg(m_roc_asic_settings.ASIC_Type, 2, 2, QChar('0'));
  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  str = QString("%1").arg(m_roc_asic_settings.chipID, 8, 2, QChar('0'));

  MapString.replace(pos, str.size(), str);
  pos+=str.size();

  return MapString;

}
//// ------------------------------------------------------------------------ //
void ConfigHandlerRocASIC::Load_ROC_ASIC_Configuration(ROC_ASIC_Setting & global,
						       ROC_ASIC_analog_epll & vmm0_epll,
						       ROC_ASIC_analog_epll & vmm1_epll,
						       ROC_ASIC_analog_epll & tds_epll,
						       ROC_ASIC_analog_epll & int_epll)
{
  m_roc_asic_settings = global;
  m_roc_asic_analog_epll_VMM0 = vmm0_epll;
  m_roc_asic_analog_epll_VMM1 = vmm1_epll;
  m_roc_asic_analog_epll_TDS  = tds_epll;
  m_roc_asic_analog_epll_int  = int_epll;
  return;
}
// -------------------------------------------------------------------------- //
