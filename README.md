# NSW Electronics - sTGC Readout Software

# //-----------------------------------------------------------------------//
#   Contents
* [How to Run](#how-to-run)
  * [Start a Run and Take Data](#start-run-and-take-data) 
* [Requirements](#requirements)
* [Installation](#installation)
  * [Obtain the software](#obtain-the-software)
  * [Compiling the software](#compile-the-software)
  * [Run the DAQ](#run-the-daq)
 
  * [installing C++](#Installing-C++)
  * [ROOT](#installing-root)
  * [Qt](#installing-qt)
  * [Boost](#installing-boost)
     * [Step-by-step](#steps-to-install-boost)
* [Useful Links](#useful-links)
* [Contact Information](#contact)

## How to Run 

First setup the MiniDAQ system and connect the eithernet port on the KC705 board to the eithernet port on the computer.  The KC705 must be connected to the network where computer host IP is 168.192.0.16.  The sockets 7201 and 6008 must be open and unblocked from the firewall.  If you're getting the "timeout waiting for reply" error. please check your connect and your firewall settings.

Open the GUI by entering the build directory
```
 cd ./build/
 ./stgc-dcs

```

### Start Run and Take Data

The main page is the "Main Run Control" tab.  All other tabs are for expert use.  You will probably also need to use 

Begin a run by specifying the configuration and output directories.

Detailed presentations on run procedure with screenshots can be found here:
https://indico.cern.ch/event/693304/contributions/2845167/
and here
https://indico.cern.ch/event/693304/contributions/2845205/

A default set of configurations can be found in:

```
./sTGC_readout_sw/readout_configuration/configuration/Default
```

Directories can be opened by clicking on the blue folder icon to the right of the input line.

The default output directory is 

```
./sTGC_readout_sw/output
```

Once a run is started, a run directory will be made in the form of `run_####`, where `####` is the run number specified in the run number box.  The run directory will store the raw data in the form of a `.bin` file.  This can be decoded later using a separate decoder in the `./sTGC_readout_sw/raw_decoder` directory.

The configuration files used for the run and the start and end time of the run are also stored in the run directory. 

Please note, if a run number that already has a run directory is selected then the old run directory will be moved to `./sTGC_readout_sw/output/previous_run`.  This will remove any runs with the same run number in the `previous_run` directory so you only get one chance to recover an old run that you accidentally over-ridden.

After specifying the configuration and output directories and the run number click the buttons in the following order:

```
Open Communications
Configure MiniDaq
Configure FEBs
Prepare Run
Start Run
```

Some steps may take a few minutes because of the many configuration commands being sent to the MiniDAQ system and FE boards.

At this point
The start time of the run should be displayed and the N Events Recorded output should update every few seconds.
The trigger rate in Hz should also be displayed and updated.

And when ready to stop the run click:

```
Stop Run
```

then go into your output run directory and look at the raw data file:  Each line cooresponds to a data packet.

```
./sTGC_readout_sw/output/run_####/run_####_raw.bin
```

## Requirements

Here we list the (tested) software requirements.

* [Qt 5.7](https://www.qt.io/qt5-7/)
* ROOT 5.34 (will not work with ROOT >=6)
* Boost 1.60
* C++11 (gcc >=4.7)

## Installation
There are a few steps that need to be taken in order for you to obtain, install, and get the DAQ software running. These are discussed here.

### Obtain the software

The nominal use case is to check out the recommended tag of the software. As there currently is no recommended tag, the suggestion is to check out the **master** branch of the software. To do this, run the following command from a terminal in a directory where you would like the DAQ software to reside:

```
git clone -b master https://gitlab.cern.ch/ssun/sTGC_readout_sw.git
```
If you are an editor of the software or would like to make commits/pull-requests to the software, instead of the above command you should use the ssh protocol to check out the code:
```
git clone -b master ssh://git@gitlab.cern.ch:8443/ssun/sTGC_readout_sw.git
```

### Compile the software

**note** if you do not have gcc>=4.7 the compilation steps outlined here will fail. If you are able (on  a CERN machine or otherwise) you can follow the steps outline in the [next section](#installation-on-a-cern-linux-machine) to obtain the correct gcc.

We next need to compile the libraries for ROOT to handle the ```vector<vector<int>>``` container class (used in the output data ntuples), build a MakeFile for the DAQ software, and compile the software. The first of these is handled by **rootcint** and the second by Qt's **qmake**. The requisite commands to do this are handled by bash script and sets you up to compile the software by calling **make** from within the ```sTGC_readout_sw/build/``` directory.

**Before doing this** you must first be sure that the  boostlib and boostinclude [5] variables are pointing to the correct location relevant to your machine. Edit them accordingly (and [obtain boost](#installing-boost) if needed)  and execute these commands:
```
cd sTGC_readout_sw/
source install.sh
make 2>&1 |tee stgc_dcs_compile.log
```

If there are any errors during compilation that are unable to be resolved, inspect the log file you generated during compilation ("stgc_dcs_compile.log") and send it to us and/or report the bug/error.

### Run the DAQ

Once the compilation begun by the call to ```make``` is complete you will have an executable ```stgc-dcs``` that can be executed to open up the DAQ GUI. On Linux machines this executable, by default, will be located in ```sTGC_readout_sw/build/```. 

```
cd build
./stgc-dcs
```
Which will promptly open up the DAQ panel.

## Setting up the Network and Opening Ports in the Firewall

set IP and configure ethernet port for your PC (fixed to 192.168.0.16 now).  Add a network profile with this IP specifically for DAQ purposes.  Make sure the connection is to this profile when connecting to the KC705.

```
Host PC IP:       192.168.0.16
Host PC  Port:    7201
Dest(KC705) IP:   192.168.0.1
Dest(KC705) Port: 6008
```

Make sure the UDP port for communicating with the KC705 FPGA board is open. For CENT OS 7,
login as super user and use the following command:

```
firewall-cmd --permanent --zone=public --add-port=6008/udp
firewall-cmd --permanent --zone=public --query-port=6008/udp
firewall-cmd --reload
```

If your computer is part of an university or lab network, you may have to ask your network security expert for help with this.  If you're not getting a reply from the KC705 at the beginning, its most likely because of a network connection problem.

## Installing C++

If your gcc compiler is the wrong version please obtain the requisite version of gcc that is able to compile against C++11 (gcc>=4.7). It is suggested to use the [CERN devtoolset][8] to update your compiler (among other things). We have tested version 1.1 and so here we provide you the commands to obtain this:
```
yum install devtoolset-1.1
scl enable devtoolset-1.1 bash
```

To be sure that you now have the correct version of gcc, run:
```
gcc --version
```
which should tell you that the current version of gcc is 4.7.2.

If, later on or from a new terminal window, you call ```gcc --version``` and the version of gcc reported is not 4.7.2 you either need to call directly
```
scl enable devtoolset-1.1 bash
```
once again or add this line to your ```~/.bashrc```. There may be other options but these are what we have used.

## Installing ROOT

If you do not have ROOT on your system or you do, but not the correct version as required for **sTGC_readout_sw**, then you
will have to get the ROOT version required from the main [ROOT pages](https://root.cern.ch/).

Downloading and setting up ROOT can take between 5 and 30 minutes on a machine with adequate internet connection.

Detailed instructions on how to download and setup the correct version of ROOT are described in this set of
[ROOT installation slides](https://twiki.cern.ch/twiki/pub/Atlas/NSWVmmDaqSoftware/root_installation_PDF.pdf). Please
use these instructions to setup ROOT for use with **sTGC_readout_sw** if you do not already have the correct
version of ROOT.

## Installing Qt

If you do not have Qt on your system or you do, but not the correct version as required for **sTGC_readout_sw**, then you will
have to install Qt from the main [Qt pages](https://www.qt.io/qt5-7/). 

The full installation procedure of Qt takes less than 1 hour on a machine with adequate internet connection.

Detailed instructions on how to download, setup the installation, and install Qt are described in this set of
[Qt installation slides](https://twiki.cern.ch/twiki/pub/Atlas/NSWVmmDaqSoftware/qt_installation_PDF.pdf). Please
use these instructions to setup Qt for use with **sTGC_readout_sw**.

## Installing Boost

Yes this package requires [Boost][3]. Several uses of Boost are perhaps now replaced by standard C++, but nevertheless, Boost is used.

**The recommended version is Boost 1.60**

### Steps to install Boost

**IMPORTANT NOTE**

It is **highly recommended** to place the installation of Boost, i.e. the directory that you provide as "**path-to-install-directory**" below, in
a location that is near **sTGC_readout_sw** or at least in a location that you will remember as being for external dependencies of
**sTGC_readout_sw**. For example, see slide 10 in the [Qt installation slides](https://twiki.cern.ch/twiki/pub/Atlas/NSWVmmDaqSoftware/qt_installation_PDF.pdf).
If you use the directory structure as described on those slides, then it is unambiguous which Boost is being used to compile **sTGC_readout_sw**.
This requires you to copy the downloaded *tar.gz* file to this install directory and proceeding with steps 1-4 below in *that directory* and using the following
```--prefix``` command:

```
--prefix=${PWD}
```
**Installation steps:**

1.  Obtain the *tar.gz [source][4] for Boost and untar the file.
If you have ```wget``` on your machine (standard for Linux) you can simply do:
```
wget http://sourceforge.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.gz
```
to get the *tar.gz file.

2.  Run the following commands to setup the build configuration:
```
cd boost_1_60_0
./bootstrap.sh --with-libraries=atomic,chrono,date_time,exception,filesystem,system,thread,timer --prefix=<install-directory>
```
You should specify a directory for the Boost installation using ```--prefix``` (```--prefix``` tells where to install Boost). In this way, you have control and know exactly where the installation
is happening. As stated above, it is recommended to use ```--prefix=${PWD}``` once you have moved the un-tarred Boost directory to a suitable location.

3.  Run the following commands to install Boost with the configuration set in step 2:
```
./b2 install threading=multi --layout=tagged --prefix=<path-to-install-directory>
```
In Step 2 we tell it only to build the required libraries (most of what we use in Boost is header-only and we only need the few compile-necessary items above) and with this subset of Boost, the install takes O(5) minutes (at least on my personal lap-top).

4.  At the end of the install after Step 3, you should have the following directories:
```
<install-directory>
<install-directory>/lib
```
The first of the above needs to be set as your [boostinclude variable][5] in [sTGC_readout_sw/build/stgc-dcs.pro][6]. The second of the above needs to be set as your [boostlib variable][7] in [sTGC_readout_sw/build/stgc-dcs.pro][6]
**For example**, if we installed boost to the directory ```stgc_daq/boost_1_60/``` under which there *should* be the directory ```boost/``` which contains a bunch of files
ending in ```.hpp``` (these are the header files for boost) and a directory ```lib/```, then in ```stgc-dcs.pro``` you should have the lines:

```
    boostinclude=<full-path>/stgc_daq/boost_1_60/
    boostlib=<full-path>/stgc_daq/boost_1_60/lib
```


## Useful links

   * [NSWVmmDaqSoftware twiki][1]
   * [NSWelectronics twiki][2]

## Contact

Questions, comments, suggestions, or help?

**Siyuan Sun**: <siyuan.sun@cern.ch>

for questions on VERSO which this program is based on, please contact:

**Daniel Antrim**: <daniel.joseph.antrim@cern.ch>

**George Iakovidis**:  <george.iakovidis@cern.ch>




[1]: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/NSWVmmDaqSoftware
[2]: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/NSWelectronics
[3]: http://www.boost.org/
[4]: http://www.boost.org/users/history/version_1_60_0.html
[5]: https://gitlab.cern.ch/ssun/sTGC_readout_sw/blob/master/build/stgc-dcs.pro#L24
[6]: https://gitlab.cern.ch/ssun/sTGC_readout_sw/blob/master/build/stgc-dcs.pro
[7]: https://gitlab.cern.ch/ssun/sTGC_readout_sw/blob/master/build/stgc-dcs.pro#L25
[8]: http://linux.web.cern.ch/linux/devtoolset/


