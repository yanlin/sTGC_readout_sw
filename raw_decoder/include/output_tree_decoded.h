//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jul 17 17:43:14 2017 by ROOT version 5.34/36
// from TTree output_tree_decoded/output_tree_decoded
// found on file: testrun.root
//////////////////////////////////////////////////////////

#ifndef output_tree_decoded_h
#define output_tree_decoded_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Fixed size dimensions of array or collections stored in the TTree if any.

class output_tree_decoded {
public :

   // Declaration of leaf types
   TString         *m_ip;
   vector<int>     *m_tdo;
   vector<int>     *m_pdo;
   vector<int>     *m_chan;
   vector<int>     *m_vmm_id;
   vector<int>     *m_bcid_rel;
   vector<int>     *m_flag;
   vector<int>     *m_board_id;
   vector<int>     *m_bcid;
   vector<int>     *m_l1_id;

   // List of branches
   TBranch        *b_m_ip;   //!
   TBranch        *b_m_tdo;   //!
   TBranch        *b_m_pdo;   //!
   TBranch        *b_m_chan;   //!
   TBranch        *b_m_vmm_id;   //!
   TBranch        *b_m_bcid_rel;   //!
   TBranch        *b_m_flag;   //!
   TBranch        *b_m_board_id;   //!
   TBranch        *b_m_bcid;   //!
   TBranch        *b_m_l1_id;   //!

   output_tree_decoded();
   virtual ~output_tree_decoded();
   virtual void     InitSelf();
};

#endif

#ifdef output_tree_decoded_cxx
output_tree_decoded::output_tree_decoded() : TTree(0) 
{
   InitSelf();
}

output_tree_decoded::~output_tree_decoded()
{
}

void output_tree_decoded::InitSelf()
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   m_ip = 0;
   m_tdo = 0;
   m_pdo = 0;
   m_chan = 0;
   m_vmm_id = 0;
   m_bcid_rel = 0;
   m_flag = 0;
   m_board_id = 0;
   m_bcid = 0;
   m_l1_id = 0;
   // Set branch addresses and branch pointers

   this->SetBranchAddress("m_ip", &m_ip, &b_m_ip);
   this->SetBranchAddress("m_tdo", &m_tdo, &b_m_tdo);
   this->SetBranchAddress("m_pdo", &m_pdo, &b_m_pdo);
   this->SetBranchAddress("m_chan", &m_chan, &b_m_chan);
   this->SetBranchAddress("m_vmm_id", &m_vmm_id, &b_m_vmm_id);
   this->SetBranchAddress("m_bcid_rel", &m_bcid_rel, &b_m_bcid_rel);
   this->SetBranchAddress("m_flag", &m_flag, &b_m_flag);
   this->SetBranchAddress("m_board_id", &m_board_id, &b_m_board_id);
   this->SetBranchAddress("m_bcid", &m_bcid, &b_m_bcid);
   this->SetBranchAddress("m_l1_id", &m_l1_id, &b_m_l1_id);
}

   return kTRUE;
}


#endif // #ifdef output_tree_decoded_cxx
