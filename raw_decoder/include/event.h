#ifndef EVENT_H
#define EVENT_H

#include "include/hit.h"

#include <vector>
#include <string>
#include <cstdint>

#include <stdint.h>
#include <TMath.h>

class event {

    public :
        event();
        virtual ~event(){};

        int EVID;
	int nVMM_hits;
        /////////////////////////////////////////
        //      
        /////////////////////////////////////////
	std::vector<uint32_t> *udp_id;
	std::vector<uint32_t> *bcid;
	std::vector<uint32_t> *bcid_rel;
	std::vector<uint32_t> *elink_id;
	std::vector<uint32_t> *vmm_id;
	std::vector<uint32_t> *chan_id;
	std::vector<uint32_t> *tdo;
	std::vector<uint32_t> *pdo;

	hit getHit( int ihit );
	void addHit( hit m_hit );

        void clear();
        bool ok; // loading went ok
};

#endif
