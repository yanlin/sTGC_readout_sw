#ifndef MONITORING_H
#define MONITORING_H

#include "TChain.h"
#include "TTree.h"
#include "TROOT.h"
#include "TCanvas.h"
#include "TPostScript.h"
#include "TH1F.h"
#include "TInterpreter.h"

#include "include/output_tree.h"
#include "include/vmm_decoder.h"

#include "include/hit.h"
#include "include/event.h"

#include <fstream>
#include <sstream>
#include <string>
#include <iostream>
#include <vector>

#include <cstdint>
#include <stdint.h>


class monitoring
{

 public :

  monitoring();
  ~monitoring();

  void setFilenameOut( std::string file ) { filename_out    = file; }
  void setNMaxEvents( int nevents )       { nMaxEvents      = nevents; }
  void setInputRoot( std::string file )   { input_file_root = file; }
  void setInputRaw( std::string file )    { input_file_raw  = file; }
  void setInputBinary( std::string file ) { input_file_binary = file; }
  void setOutPs( std::string file )       { out_ps = file; }

  void InitOutput();
  void InitHists();

  void RunInputRoot();
  void RunInputRaw();
  void RunInputBinary();

  void Decode_and_Fill( std::vector<uint32_t> datagram );
  void fillEvent();

  void Draw_and_Save();
  void End();
  
  void setupOutputTree( TTree* tree );
  void setupOutputEventTree( TTree* tree );
  

 private :

  int nMaxEvents;
  int ievent;

  event m_event;
  hit   m_hit;
  
  std::string filename_out;
  std::string input_file_root;
  std::string input_file_raw;
  std::string input_file_binary;
  std::string out_ps;
  
  TFile *outfile;
  TCanvas *c1;
  TPostScript *ps;

  TTree * decoded_tree;
  TTree * decoded_event_tree;
  VMMDecoder *decoder;

  //----------------------------------------------------//
  //               Vectors of histograms                //
  //----------------------------------------------------//

  // indexes: ELink #, VMM #, channel #;

  std::vector<std::vector<TH1F*>> channel_hits;

  std::vector<std::vector<int>> active_chans;
  std::vector<std::vector<int>> active_vmms;
  std::vector<std::vector<std::vector<TH1F*>>>    channel_pdo;
  //  std::vector<std::vector<std::vector<TH1F*>>>    channel_tdo;
  std::vector<std::vector<std::vector<TH1F*>>>    channel_l1id;
  std::vector<std::vector<std::vector<TH1F*>>>    channel_diff_bcid;

  //----------------------------------------------------//
  //                 Setup Output TTree
  //----------------------------------------------------//

  TString         m_ip;
  int             m_tdo;
  int             m_pdo;
  int             m_chan;
  int             m_vmm_id;
  int             m_bcid_rel;
  int             m_flag;
  int             m_board_id;
  int             m_bcid;
  int             m_l1_id;
  int             m_elink_id;

  TBranch        *b_m_ip;   //!            
  TBranch        *b_m_tdo;   //!           
  TBranch        *b_m_pdo;   //!           
  TBranch        *b_m_chan;   //!          
  TBranch        *b_m_vmm_id;   //!        
  TBranch        *b_m_elink_id;
  TBranch        *b_m_bcid_rel;   //!      
  TBranch        *b_m_flag;   //!         
  TBranch        *b_m_board_id;   //!     
  TBranch        *b_m_bcid;   //!         
  TBranch        *b_m_l1_id;   //!         

  //--------------------------------------------------//

  std::vector<uint32_t> *m_udp_id;
  std::vector<uint32_t> *m_vec_bcid;
  std::vector<uint32_t> *m_vec_bcid_rel;
  std::vector<uint32_t> *m_vec_elink_id;
  std::vector<uint32_t> *m_vec_vmm_id;
  std::vector<uint32_t> *m_vec_chan;
  std::vector<uint32_t> *m_vec_tdo;
  std::vector<uint32_t> *m_vec_pdo;

  int m_evid;
  int m_nhits;

};

#endif
