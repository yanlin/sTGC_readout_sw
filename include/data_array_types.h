#ifndef CONSTANTS_AND_TYPES_H
#define CONSTANTS_AND_TYPES_H

//boost
#include <boost/array.hpp>

//std/stl
#include <string>
#include <vector>


//#define MAX_UDP_LEN 65507 // maximum number of bytes in a given UDP datagram to be passed to event_builder
#define MAX_UDP_LEN 2500

/*
    RawDataArray

    Define a type for the array that holds the raw data
    received in each UDP packet prior to any decoding.
*/
typedef boost::array<uint32_t, MAX_UDP_LEN> RawDataArray;


/*
    IPDataArray

    Define a type for the data queue in daq_server. The data
    being queued should be associated with the IP that sent
    the data so that at the time when this element is dequeued
    from the buffer it can still be associated with this IP
    and this IP can also be sent to event_builder
*/
typedef std::pair<std::string, std::vector<uint32_t> > IPDataArray;
//typedef std::pair<std::string, RawDataArray> IPDataArray;


#endif
