#ifndef CALIB_MODULE_VMM_H
#define CALIB_MODULE_VMM_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include <string>
#include <sstream>
#include <fstream>
#include <stdio.h>

#include <QString>

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TF1.h>
#include <TGraph.h>

// std/stdl
#include <iostream>
#include <fstream>

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class Calib_VMM 
{
    public :
         Calib_VMM();
         virtual ~Calib_VMM(){};

	 std::vector<uint32_t> get_opt_trim_bits() { return m_opt_trim_bit; }
	 
	 std::vector<uint32_t> find_Opt_Trim_Bit();

	 void write_Calib_VMM_Histos(QString outfile, int iboard);
	 void write_ThDAC_Calib_VMM_Histos(QString outfile, int iboard);

	 void Calc_Baseline_Avg();
	 void Calc_Baseline_RMS();

	 void setDebug( bool dbg ) { m_dbg = dbg; }
	 bool dbg() { return m_dbg; }

	 std::vector<std::vector<uint32_t>> m_chan_baseline_vs_trim;
	 std::vector<std::vector<std::vector<uint32_t>>> m_chan_baseline_trials;

	 std::vector<std::vector<float>> m_chan_baseline_mean;
	 std::vector<std::vector<float>> m_chan_baseline_rms;
	 
	 std::vector<std::vector<uint32_t>> m_threshold_dac_adc_per_vmm;
	 std::vector<std::vector<uint32_t>> m_threshold_dac_val_per_vmm;

	 std::vector<std::vector<std::vector<uint32_t>>> m_threshold_dac_adc_per_vmm_trials;

    private :
	
	std::vector<uint32_t> m_opt_trim_bit;
	bool m_dbg;
	std::stringstream m_sx;

}; // class ConfigHandler



#endif
