#ifndef STGCSOCKET_H
#define STGCSOCKET_H

/////////////////////////////////////////
//
// vmm_socket
//
// Wrapper around Qt QUdpSocket
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// Qt
#include <QUdpSocket>

// vmm
#include "message_handler.h"

/////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------------- //
//  sTGCSocket
// ----------------------------------------------------------------------- //
/////////////////////////////////////////////////////////////////////////////

class sTGCSocket : public QObject
{
    Q_OBJECT;

    public :
        explicit sTGCSocket(QObject *parent = 0);
        virtual ~sTGCSocket(){};

        sTGCSocket& setDebug(bool dbg) { m_dbg = dbg; return *this; }
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

        void setName(std::string n = "") { m_name = n; }
        std::string getName() { return m_name; }
        void setBindingPort(quint16 port) { m_bindingPort = port; }
        quint16 getBindingPort() { return m_bindingPort; }
	void setBindingIp(QString HostIp) { m_HostIp = HostIp; }
	QString getBindingIP() { return m_HostIp; }

        // QUdpMethods
        virtual bool hasPendingDatagrams();
        virtual quint64 pendingDatagramSize();
        virtual quint64 readDatagram(char* databuffer, quint64 maxSize,
				     QHostAddress* address, quint16* port);

        bool bindSocket(const QHostAddress & IPaddress, quint16 port = 0,
            QAbstractSocket::BindMode mode = QAbstractSocket::DefaultForPlatform);
        bool isBound();
        quint64 writeDatagram(const QByteArray& data, const QHostAddress& host,
                    quint16 port);


        bool checkAndReconnect(std::string fromWhere="");
        void closeAndDisconnect(std::string fromWhere="");


        QUdpSocket& socket() { return *m_socket; }
        QByteArray buffer() { return m_buffer; }
        QByteArray processReply(QHostAddress *target_ip, quint16 *target_port);

        void Print();

    signals :
        void dataReady();

    public slots :
        void readyRead();

    private :
        bool m_dbg;
        MessageHandler *m_msg;
        std::string m_name;
        quint16 m_bindingPort;
        QUdpSocket *m_socket;
        QByteArray m_buffer;
	QString m_HostIp;

}; // class sTGCSocket

#endif
