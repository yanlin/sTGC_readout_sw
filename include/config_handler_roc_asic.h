#ifndef CONFIG_HANDLER_ROC_ASIC_H
#define CONFIG_HANDLER_ROC_ASIC_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

#include "message_handler.h"

#include "roc_asic_setting.h"
#include "roc_asic_analog_epll.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerRocASIC : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerRocASIC(QObject *parent = 0);
        virtual ~ConfigHandlerRocASIC(){};

        ConfigHandlerRocASIC& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

	void SetBoardID( int boardID );
	void SetBoardType( int boardType );

	bool Load_ROC_ASICConfig_from_File(const QString &filename);
        void Write_ROC_ASICConfig_to_File(QString filename);

	ROC_ASIC_Setting     Load_ROC_ASIC_Settings(       const boost::property_tree::ptree& p);
	ROC_ASIC_analog_epll Load_ROC_ASIC_Ana_VMM0_Config(const boost::property_tree::ptree& p);
	ROC_ASIC_analog_epll Load_ROC_ASIC_Ana_VMM1_Config(const boost::property_tree::ptree& p);
	ROC_ASIC_analog_epll Load_ROC_ASIC_Ana_TDS_Config( const boost::property_tree::ptree& p);
	ROC_ASIC_analog_epll Load_ROC_ASIC_Ana_Int_Config( const boost::property_tree::ptree& p);

	ROC_ASIC_analog_epll Load_ROC_ASIC_Ana_ePll(const boost::property_tree::ptree& pt, int iEPll);

        // methods for GUI interaction
	void Load_ROC_ASIC_Configuration(ROC_ASIC_Setting & global, 
					 ROC_ASIC_analog_epll & vmm0_epll, ROC_ASIC_analog_epll & vmm1_epll, 
					 ROC_ASIC_analog_epll & tds_epll,  ROC_ASIC_analog_epll & int_epll);
	//	void Set_ROC_Registered_to_Default(int boardID, int boardType);

	ROC_ASIC_Setting     & ROC_ASIC_Settings() {          return m_roc_asic_settings; }
	ROC_ASIC_analog_epll & ROC_ASIC_Ana_VMM0_Settings() { return m_roc_asic_analog_epll_VMM0; }
	ROC_ASIC_analog_epll & ROC_ASIC_Ana_VMM1_Settings() { return m_roc_asic_analog_epll_VMM1; }
	ROC_ASIC_analog_epll & ROC_ASIC_Ana_TDS_Settings()  { return m_roc_asic_analog_epll_TDS; }
	ROC_ASIC_analog_epll & ROC_ASIC_Ana_Int_Settings()  { return m_roc_asic_analog_epll_int; }

	QString ROC_ASICMask();

    private :
        bool m_dbg;

	boost::property_tree::ptree ROC_ASIC_Ana_ePll_pTree(ROC_ASIC_analog_epll& Analog_ePll_obj);

	ROC_ASIC_Setting         m_roc_asic_settings;
	ROC_ASIC_analog_epll     m_roc_asic_analog_epll_VMM0;
	ROC_ASIC_analog_epll     m_roc_asic_analog_epll_VMM1;
	ROC_ASIC_analog_epll     m_roc_asic_analog_epll_TDS;
	ROC_ASIC_analog_epll     m_roc_asic_analog_epll_int;

        MessageHandler* m_msg;

	const static int VMM0_ePll = 0;
	const static int VMM1_ePll= 1;
	const static int TDS_ePll= 2;
	const static int Int_ePll= 3;

}; // class ConfigHandler



#endif
