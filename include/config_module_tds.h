#ifndef CONFIG_MODULE_TDS_H
#define CONFIG_MODULE_TDS_H

/////////////////////////////////////////
//
// configuration_module
//
// Tool for building and sending the
// configuration packets to the front-end/
// VMMs
//
//  - sends the global SPI and configuration
//    of all the VMM channels
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

#include "config_module_base.h"
#include "config_handler_tds.h"
#include "socket_handler.h"
#include "message_handler.h"

// Qt
#include <QString>


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  Config_TDS
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////

class Config_TDS : public Config_Base
{

    public :
        explicit Config_TDS(Config_Base *parent = 0);
        virtual ~Config_TDS(){};

        Config_TDS& LoadConfig(ConfigHandlerTDS& config);

        bool SendTDSConfig(int send_to_port, const QHostAddress & target_ip);

	void fillTDSGlobalRegisters(std::vector<QString>& globalRegisters);
        void fillTDSChannelRegisters(std::vector<QString>& channelRegisters);
        void fillTDSLutRegisters(std::vector<QString>& lutRegisters);

        ConfigHandlerTDS& config() { return *m_configHandler; }

 private :

	ConfigHandlerTDS *m_configHandler;
	
 signals :

	public slots :
       
}; // class Config_TDS



#endif
