#ifndef DATA_HANDLER_H
#define DATA_HANDLER_H

#include <QObject>

//std/stl                                                                                                                                                   
#include <sstream>

//boost                                                                                                                                                     
//DaqBuffer                                                                                                                                                 
//#include <boost/array.hpp>                                                                                                                                
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
//#include <boost/atomic.hpp>                                                                                                                               

#include <TString.h>
#include <TFile.h>
#include <TTree.h>

#include <string.h>

//nsw                                                                                                                                                       
//#include "event_builder.h"
#include "data_array_types.h" // RawDataArray                                                                                       
#include "vmm_decoder.h"
#include "daq_server.h"
                        
//#include "calibration_module.h"
//#include "daq_monitor.h"
//class MapHandler;
//class OnlineMonTool;
class MessageHandler;
//class CalibrationState;
class DaqBuffer;

class DataHandler : public QObject
{
    Q_OBJECT

 public :
    explicit DataHandler(QObject *parent=0);
    virtual ~DataHandler(){};

    void LoadMessageHandler(MessageHandler& msg);
    MessageHandler& msg() { return *m_msg; }

    void setDebug(bool dbg);
    void initialize();
    void gather_data();

    void setOutDir( std::string outDir ) { m_outDir = outDir; }
    void setRunNumber( std::string irun )        { m_run_number = irun; }

    void setRunStartTime(std::string starttime);
    void setRunEndTime(std::string endtime);

    void setFilenameRaw(std::string filename);
    void setFilenameMeta(std::string filename);
    void setFilenameDecoded(std::string filename);

    void setOutputDecoded(bool setOut);
    void setOutputRaw(bool setOut);

    std::vector<bool> current_locked_elinks();
    uint32_t          current_EVID();
    uint32_t          current_TTC_status();

    void setEvtCount(int count);
    void set_run_maxevents( int run_max_evt ); 

    //DaqBuffer                                                                                                       
    std::vector<uint32_t> current_TTC_reg_address() { return m_server->current_TTC_reg_address(); }
    std::vector<uint32_t> current_TTC_reg_value()   { return m_server->current_TTC_reg_value(); }
                                  
    void endRun();

 private :
    MessageHandler* m_msg;
    bool m_dbg;
    std::stringstream m_sx;

    DaqServer *m_server;

    std::string m_outDir;
    std::string m_run_number;

};

#endif
