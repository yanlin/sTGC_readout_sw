/////////////////////////////////////////                                                                                   
//                                                                                                                          
// mainwindow                                                                                                               
//                                                                                                                          
// Main GUI for sTGC DAQ/DCS                                                                                                
//                                                                                                                          
// siyuan.sun@cern.ch,                                                                                                      
// June 2017                                                                                                                
//                                                                                                                          
// based on code written by daniel.joseph.antrim@cern.ch                                                                    
// for vmm_readout_sw                                                                                                       
//////////////////////////////////////////     

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// qt
#include <QMainWindow>
#include <QScrollArea>
#include <QGridLayout>
#include <QPushButton>
#include <QComboBox>
#include <QFont>
#include <QThread>
#include <QRadioButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QDateTime>
#include <QTimer>
#include <QTime>

#include <QStringList>
#include <QFileInfoList>
#include <QMessageBox>
#include <QDir>
#include <QList>
class QListWidget;
class QListWidgetItem;

// vmm
#include "config_handler_miniDAQ.h"
#include "config_handler_board.h"
#include "config_handler_roc_fpga.h"
#include "config_handler_roc_asic.h"
#include "config_handler_vmm.h"
#include "config_handler_tds.h"

#include "config_module_base.h"
#include "config_module_board.h"
#include "config_module_roc_fpga.h"
#include "config_module_roc_asic.h"
#include "config_module_vmm.h"
#include "config_module_tds.h"

#include "calib_module_vmm.h"

#include "socket_handler.h"
#include "message_handler.h"

#include "data_handler.h"
//#include "daq_monitoring.h"

//#include "global_setting.h"
//#include "comm_info.h"
//#include "channel.h"

#include <string>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

        bool dbg() { return m_dbg; }

        QFont Font;
        QWidget* dummy;

        bool configOK() { return m_configOK; }

        //////////////////////////////////////////////////////
        // methods to grab the VMM tools
        //////////////////////////////////////////////////////
        ConfigHandlerBoard&   BoardConfigHandle()   { return *boardConfigHandler; }
        ConfigHandlerRocFPGA& RocFPGAConfigHandle() { return *rocFPGAConfigHandler; }
        ConfigHandlerRocASIC& RocASICConfigHandle() { return *rocASICConfigHandler; }
	ConfigHandlerVMM&     VMMConfigHandle()     { return *vmmConfigHandler; }
	ConfigHandlerTDS&     TDSConfigHandle()     { return *tdsConfigHandler; }
	ConfigHandlerMiniDAQ& miniDAQConfigHandle() { return *miniDAQConfigHandler; }

        SocketHandler& socketHandle()     { return *stgcSocketHandler; }

        Config_Base&  BaseConfigModule()  { return *baseConfigModule;  }
        Config_Board& BoardConfigModule() { return *boardConfigModule;  }
	Config_RocFPGA& RocFPGAConfigModule() { return * rocFPGAConfigModule; }
        Config_RocASIC& RocASICConfigModule() { return * rocASICConfigModule; }
        Config_VMM&   VMMConfigModule()   { return *vmmConfigModule;  }
        Config_TDS&   TDSConfigModule()   { return *tdsConfigModule;  }

	Calib_VMM&    VMMCalibModule()    { return *vmmCalibModule; }

        MessageHandler& msg()             { return *stgcMessageHandler; }
    
	DataHandler&   dataHandle()       { return *vmmDataHandler;    }

	//---------------------------------------------//

        //////////////////////////////////////////////////////
        // IPs
        //////////////////////////////////////////////////////

        void buildHostIP();
	void buildTargetIP();
        void IPaddressChange();
	std::string getRunDir();

	void SetInitialState();
        QGridLayout *VMMchannelGridLayout;
        void CreateVMMChannelsFields();
        void CreateVMMChannelsFieldsMulti();
	void InitializeVMMChannelArrayValues();

	QString GetCurrentTimeStr();
	//	void    PopUpError(const char * str, std::string info);
	void    PopUpError(const char * str, const char * info="");
	void    PopUpError(QString str, QString info);
        void    PopUpWarning(const char * str, const char * info="");
        void    PopUpWarning(QString str, QString info);
        void    PopUpInfo(const char * str, const char * info="");
        void    PopUpInfo(QString str, QString info);

	QGridLayout *TDSchannelGridLayout;
	void CreateTDSChannelsFieldsMulti();
	//	void InitializeTDSChannelArrayValues();

        QGridLayout *GPIOchanGridLayout;
	void CreateGPIOChanFieldsMulti();

	void DisplayLinkLockedStatus(std::vector<int> best_delay);
	void loadMiniDAQConfig_fromTTC();

        /////////////////////////////////////////////////////
        // Channel Fields Buttons
        /////////////////////////////////////////////////////
        QLineEdit *VMMChannel[64];
        QComboBox *VMMSDVoltage[64];
        QComboBox *VMMSZ010bCBox[64];
        QComboBox *VMMSZ08bCBox[64];
        QComboBox *VMMSZ06bCBox[64];
    
        QPushButton *VMMSP[64];
        QPushButton *VMMNegativeButton[64];
        QPushButton *VMMSC[64];
        QPushButton *VMMSL[64];
        QPushButton *VMMST[64];
        QPushButton *VMMSTH[64];
        QPushButton *VMMSM[64];
        QPushButton *VMMSMX[64];
    
	QComboBox *TDSChanDelay[104];
	QComboBox *TDSChanEnableBox[128];

	//----------------------------------------------//

	std::vector<QRadioButton*> SROC0_VMM_Ena;
	std::vector<QRadioButton*> SROC1_VMM_Ena;
	std::vector<QRadioButton*> SROC2_VMM_Ena;
	std::vector<QRadioButton*> SROC3_VMM_Ena;

	std::vector<QRadioButton*> ROC_VMM_Ena;

	std::vector<QRadioButton*> SROC_EOP_Ena;
	std::vector<QRadioButton*> SROC_NullEvt_Ena;
	std::vector<QRadioButton*> SROC_Ena;
	std::vector<QRadioButton*> SROC_TDC_Ena;
	std::vector<QRadioButton*> SROC_Busy_Ena;
	std::vector<QRadioButton*> ROC_fake_vmm_failure;

	std::vector<QRadioButton*> VMM0_epll_phase_ena;
	std::vector<QRadioButton*> VMM0_ctrl_delay_ena;
	std::vector<QRadioButton*> VMM0_ctrl_bypass_ena;
	std::vector<QRadioButton*> VMM0_TP_bypass_ena;
	std::vector<QRadioButton*> VMM0_TX_ena;
	std::vector<QRadioButton*> VMM0_TX_cSEL;

	std::vector<QRadioButton*> VMM1_epll_phase_ena;
        std::vector<QRadioButton*> VMM1_ctrl_delay_ena;
        std::vector<QRadioButton*> VMM1_ctrl_bypass_ena;
        std::vector<QRadioButton*> VMM1_TP_bypass_ena;
        std::vector<QRadioButton*> VMM1_TX_ena;
        std::vector<QRadioButton*> VMM1_TX_cSEL;

	std::vector<QRadioButton*> TDS_epll_phase_ena;
        std::vector<QRadioButton*> TDS_ctrl_delay_ena;
        std::vector<QRadioButton*> TDS_ctrl_bypass_ena;
        std::vector<QRadioButton*> TDS_TP_bypass_ena;
        std::vector<QRadioButton*> TDS_TX_ena;
        std::vector<QRadioButton*> TDS_TX_cSEL;

	std::vector<QRadioButton*> ROC_int_epll_phase_ena;

	//----------------------------------------------//

	std::vector<QRadioButton*> BoardID_Linked;
	std::vector<QSpinBox*>     ELink_Manual_Phase;
	std::vector<QLineEdit*>    ELink_stat;
	std::vector<QRadioButton*> elink_lked;

        bool VMMSCBool[64];
        bool VMMSLBool[64];
        bool VMMSTBool[64];
        bool VMMSTHBool[64];
        bool VMMSMBool[64];
        bool VMMSMXBool[64];
        bool VMMSPBool[64];
        int VMMSDValue[64];
        int VMMSZ010bValue[64];
        int VMMSZ08bValue[64];
        int VMMSZ06bValue[64];
    
        bool VMMSPBoolAll;
        bool VMMSCBoolAll;
        bool VMMSLBoolAll;
        bool VMMSTBoolAll;
        bool VMMSTHBoolAll;
        bool VMMSMBoolAll;
        bool VMMSMXBoolAll;
        bool VMMSZ010bBoolAll;
        bool VMMSZ08bBoolAll;
        bool VMMSZ06bBoolAll;
    
        bool VMMSPBoolAll2;
        bool VMMSCBoolAll2;
        bool VMMSLBoolAll2;
        bool VMMSTBoolAll2;
        bool VMMSTHBoolAll2;
        bool VMMSMBoolAll2;
        bool VMMSMXBoolAll2;
        bool VMMSZ010bBoolAll2;
        bool VMMSZ08bBoolAll2;
        bool VMMSZ06bBoolAll2;

	//--------------------------------------------//

	bool TDSChannelEnableValue[128];
	int TDSChannelDelayValue[128];

	//-------------------------------------------//

	std::vector<bool> GPIO_dir_chan_enable_val;
	std::vector<bool> GPIO_dout_chan_enable_val;

	//------------------------------------------//

	QDateTime *DateTime;

	float find_avg( std::vector<uint32_t> vec ); 

    private:
        Ui::MainWindow *ui;
        bool m_dbg;

	bool stgc_bound;
	bool daq_bound;
	bool data_bound;

	int icommand;

	QMessageBox msgBox;

	const int HOSTPORT;
	const int TARGETPORT;

	QHostAddress HostIp;
	QHostAddress TargetIp;

	int  reply_category;
	bool m_reply_error;

        ConfigHandlerBoard   *boardConfigHandler;
	ConfigHandlerRocFPGA *rocFPGAConfigHandler;
	ConfigHandlerRocASIC *rocASICConfigHandler;
        ConfigHandlerVMM     *vmmConfigHandler;
        ConfigHandlerTDS     *tdsConfigHandler;
	ConfigHandlerMiniDAQ *miniDAQConfigHandler;

        SocketHandler  *stgcSocketHandler;

        Config_Base    *baseConfigModule;
	Config_Board   *boardConfigModule;
	Config_RocFPGA *rocFPGAConfigModule;
        Config_RocASIC *rocASICConfigModule;
	Config_VMM     *vmmConfigModule;
	Config_TDS     *tdsConfigModule;

	Calib_VMM      *vmmCalibModule;

        MessageHandler *stgcMessageHandler;

	DataHandler    *vmmDataHandler;

        bool m_commOK;
        bool m_configOK;
	bool ignore_channel_update;
	bool ignore_tds_channel_update;
	bool ignore_gpio_channel_update;

	void InitRocGUI();

	void LoadVMMConfig_from_Handler();
	void LoadVMMConfig_to_Handler();

        void LoadTDSConfig_from_Handler();
        void LoadTDSConfig_to_Handler();

	void Load_ROC_FPGA_Config_from_Handler();
	void Load_ROC_FPGA_Config_to_Handler();

        void Load_ROC_ASIC_Config_from_Handler();
	void Load_ROC_ASIC_Config_to_Handler();

	void LoadBoardConfig_to_Handler();
	void LoadBoardConfig_from_Handler();

	void DisableConfiguration();
	void EnableConfiguration();

	int Convert_TDS_Mask_to_Value();
        int Convert_VMM_Mask_to_Value();

	void Display_VMM_Mask(uint32_t VMMMask);
	void Display_TDS_Mask(uint32_t TDS_ID);

	void Display_Roc_Digital_Ena_Mask( int iMask, uint32_t mask );
	void Display_Roc_Analog_VMM0_epll_Ena_Mask( int iMask, uint32_t mask );
	void Display_Roc_Analog_VMM1_epll_Ena_Mask( int iMask, uint32_t mask );
	void Display_Roc_Analog_TDS_epll_Ena_Mask( int iMask, uint32_t mask );
	void Display_Roc_Analog_ROC_int_epll_Ena_Mask( int iMask, uint32_t mask );

	uint32_t Roc_Digital_Ena_Mask( int iMask );
	uint32_t Roc_Analog_VMM0_epll_Ena_Mask( int iMask );
	uint32_t Roc_Analog_VMM1_epll_Ena_Mask( int iMask );
        uint32_t Roc_Analog_TDS_epll_Ena_Mask( int iMask );
        uint32_t Roc_Analog_ROC_int_epll_Ena_Mask( int iMask );

	int QueryAllELinks( bool isSetup = false);
	std::vector<int> CheckLinkLocked( std::vector< std::vector<bool> > LinkOffsets, int & nlinks );

	std::vector<uint32_t> m_current_TTC_reg_value;
	std::vector<uint32_t> m_current_TTC_reg_address;

	bool m_is_running;
	std::vector<long> m_run_EVID_time;
	std::vector<bool> m_run_start_elinks;
	void SetManualELinkPhase();

	std::vector<std::vector<int>> m_opt_trim_bits;
	
	int prev_mon_chan;
	uint32_t m_current_SCA_ID;

	std::vector<uint32_t>              Scan_Baseline(            int ivmm, int iboard, int boardType );
	std::vector<std::vector<uint32_t>> Scan_Baseline_Multi(      int ivmm, int iboard, int boardType );
	bool                               Set_CalibrateVMM_Channel( int ichan, int ivmm, int iboard,
								     int boardType, bool trimming, int trimmingbit );
	std::vector<uint32_t>              CalibrateVMM_Channel(     int ivmm, int iboard, int boardType );
	std::vector<std::vector<uint32_t>> Scan_TrimmingThreshold(   int ivmm, int iboard, int boardType );
	void                               Find_Optimal_Trimming(    int ivmm, int board, int boardType );

    signals :
        //testMon

    public slots:
        // logging
        void readLog();

	void configSCA();
	void configBoard();
	void clickedTDS(bool checked);
	void setupMiniDAQ();
	void prepareAndSendInputConfig();
	void prepareAndSendConfigDir(  bool resetVMM_hard=false, 
				       bool resetVMM_auto=false, 
				       bool board_sync_soft_reset = false,
				       bool board_renable_gpio = false,
				       bool isSetup=true, bool onlyPFEB = false);
        void prepareAndSendBoardConfig(bool resetVMM_hard=false, 
				       bool resetVMM_auto=false, 
				       bool onlyPFEB = false);
	void prepareAndSendVMMConfig();
	void prepareAndSendTDSConfig();
	void prepareAndSendRocFPGAConfig();
        void prepareAndSendRocASICConfig();

	void configBoard_all_vmms();

	void configDir_stcrReset();
	void configDir_hardReset();
	void configBoard_stcrReset();
	void configBoard_hardReset();

	void updateLogScreen();


	void getElinkPhaseMatrix();

         // update the IP list if the IPs are changed, and force a reconnect
        void IPchanged(QString);
        void IPchanged(int);

        // connect to IP
        void Connect();
	void Sync_Soft_Reset();

	void checkRequestedFile();
	void selectVMMConfigXMLFile();
	void loadVMMConfigurationFromFile();
	void writeVMMConfigurationToFile();

        void updateVMMChannelState();
        void updateIndividualVMMChannelState();
        void updateVMMChannelVoltages(int);
        void updateVMMChannelADCs(int);

	void selectDirConfig();
	void selectOutputDir();
	void selectBoardConfigXMLFile();

	void selectTDSConfigXMLFile();
        void loadTDSConfigurationFromFile();
	void writeTDSConfigurationToFile();

	void selectRocFPGAConfigXMLFile();
	void loadRocFPGAConfigurationFromFile();
	void writeRocFPGAConfigurationToFile();

        void selectRocASICConfigXMLFile();
        void loadRocASICConfigurationFromFile();
        void writeRocASICConfigurationToFile();

	void loadBoardConfigurationFromFile();
	void writeBoardConfigurationToFile();
	void updateFEBType();

	void updateIndividualTDSChannelState();
	void updateIndividualTDSChannelDelay();
	void updateTDSChannelDelay(int index);
	void updateTDSChannelEnable(int index);

	void updateGPIOChannelEnable(int index);
	void updateIndividualGPIOChannelState();

	void loadCurrentConfigBoard();
	void loadCurrentConfigVMM();
	void loadCurrentConfigTDS();
	void loadCurrentConfigRocFPGA();
	void loadCurrentConfigRocASIC();

	void setMax();
	void sendCommand();

	void startRun();
	void enableDAQ();
	void stopRun();

        void QueryRunStatus();
        void DisplayDateTime();

	bool ConfigureTTC_toDefault();

	void CalibrateVMM();
	void FindBaseline();
	void Display_Baseline_Scan();
	void Display_Trimming_Matrix();
	
	void CalibrateBoard_ConfigVMMs( int ichan, int type, 
					int trimmingbit, int ithreshold );
	void FindBoardCalibration();
	void FindBoardBaseline();
	void FindBoardBaselineDetailed();
	void FindBoardThDacCalibration();

	void CalibWholeDir();
	std::vector<std::vector<std::vector<uint32_t>>>              CalibrateWholeBoard();
	std::vector<std::vector<uint32_t>>                           ScanBaselineWholeBoard();
	std::vector<std::vector<std::vector<uint32_t>>>              ScanBaselineWholeBoardMulti();
	std::vector<std::vector<std::vector<std::vector<uint32_t>>>> ScanThresholdDacWholeBoard();

};


#endif // MAINWINDOW_H
