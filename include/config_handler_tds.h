#ifndef CONFIG_HANDLER_TDS_H
#define CONFIG_HANDLER_TDS_H

/////////////////////////////////////////
//
// config_handler
//
// containers for global configuration
// (global, VMM channels, IP/socket information)
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

// vmm
#include "message_handler.h"

#include "tds_global_setting.h"
#include "tds_channel.h"
#include "tds_lut.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerTDS : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerTDS(QObject *parent = 0);
        virtual ~ConfigHandlerTDS(){};

        ConfigHandlerTDS& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }

	void SetBoardID( int boardID );
	void SetBoardType( int boardType );

	bool LoadTDSConfig_from_File(const QString &filename);
        void WriteTDSConfig_to_File(QString filename);

	TDSGlobalSetting LoadTDSGlobalSettings(const boost::property_tree::ptree& p);
	std::vector<TDS_Channel> LoadTDSChannelConfig(const boost::property_tree::ptree& p);
	std::vector<TDS_LUT> LoadTDSLutConfig(const boost::property_tree::ptree& p);

        // methods for GUI interaction
	void LoadTDSChipConfiguration(TDSGlobalSetting& global, std::vector<TDS_Channel>& channels,
				      std::vector<TDS_LUT>& luts);

	TDSGlobalSetting& TDSGlobalSettings()      { return m_tds_globalSettings; }
	TDS_Channel& TDS_ChannelSettings(int i) { return m_tds_channels[i]; }
	TDS_LUT& TDS_LUTSettings(int i)         { return m_tds_luts[i]; }

	QString TDSMask();

    private :
        bool m_dbg;

	TDSGlobalSetting        m_tds_globalSettings;
	std::vector<TDS_Channel> m_tds_channels;
	std::vector<TDS_LUT>     m_tds_luts;

        MessageHandler* m_msg;

}; // class ConfigHandler



#endif
