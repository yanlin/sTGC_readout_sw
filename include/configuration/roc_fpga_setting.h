#ifndef ROC_FPGA_SETTING_H
#define ROC_FPGA_SETTING_H

#include <QStringList>
#include <QList>

class ROC_FPGASetting {

    public :
        ROC_FPGASetting();
        virtual ~ROC_FPGASetting(){};

	std::string header;
	int boardID;
	int board_Type;
	int ASIC_Type;
        int chipID;
	std::string command;
	std::string UniqueS;

        uint32_t vmm_enable_mask;
	uint32_t cktp_vs_ckbc_skew;
	uint32_t cktp_width;
	uint32_t trigger_window_width;

        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
