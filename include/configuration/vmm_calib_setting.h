#ifndef VMM_CALIB_SETTING_H
#define VMM_CALIB_SETTING_H

#include <QStringList>
#include <QList>

class VMMCalibSetting {

    public :
        VMMCalibSetting();
        virtual ~VMMCalibSetting(){};

	std::vector<uint32_t> chan_baselines;
	std::vector<std::vector<uint32_t>> chan_baseline_vs_trim;
	std::vector<uint32_t> opt_trim_bits;

        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
