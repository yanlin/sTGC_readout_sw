#ifndef VMM_CHANNEL_H
#define VMM_CHANNEL_H

class VMM_Channel {

    public :
        VMM_Channel();
        virtual ~VMM_Channel(){};

        int number;
        /////////////////////////////////////////
        // VMM3 registers
        /////////////////////////////////////////
        int sc; // large sensor capacitance mode ([0] <~200pF, [1] >~200pF)
        int sl; // leakage current disable ([0] enable)
        int st; // 300 fF test capacitor ([1] enable)
        int sth;    // multiplies test capacitor by 10
        int sm; // mask enable ([1] enable)
        int sd; // trim threshold DAC, 1mV step
        int smx; // channel monitor mode ([0] analog output, [1] trimmed threshold)
        int sz10b;  // 10-bit ADC zero
        int sz8b;   // 8-bit ADC zero
        int sz6b;   // 6-bit ADC zero

        /////////////////////////////////////////
        // VMM2 registers
        /////////////////////////////////////////
        int polarity;
        int capacitance;
        int leakage_current;
        int test_pulse;
        int hidden_mode;
        int trim;
        int monitor;
        int s10bitADC;
        int s8bitADC;
        int s6bitADC;

        void print();
        bool ok; // loading went ok
};

#endif
